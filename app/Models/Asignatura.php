<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Asignatura extends Model
{
    use HasFactory;

    protected $fillable = [
        'nombre',
        'horas'
    ];


    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function alumnosMatriculados() {
        return $this->hasMany(User::class);
    }
}
