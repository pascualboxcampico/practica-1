<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Asignatura;
use Illuminate\Support\Facades\DB;

class AsignaturaController extends Controller
{
    public function getAlumnos(Request $request, $id) {
        $asignatura = Asignatura::find($id);
        $mensaje = 'Los ' . $asignatura->alumnosMatriculados->count() . ' alumnos matriculados en ' . $asignatura->nombre . ' son ' . $asignatura->alumnosMatriculados;
        $mensaje .= 'Sus nombres son: ';
        foreach($asignatura->alumnosMatriculados as $alumno) {
            $mensaje .= $alumno->name . ', ';
        }

        return $mensaje;
    }

    public function getAll(Request $request) {
        return Asignatura::all()->toJSON();
    }

    public function create(Request $request) {
        $asignatura = Asignatura::create($request->only(['nombre', 'horas']));
        return $asignatura->toJSON();
    }

    public function search(Request $request) {
        $value = $request->value; // DWC
        $operator = $request->operator; // 'like'
        $fields = $request->fields; // ['horas', 'nombre']

        switch ($operator) {
            case 'like':
                $value = "%$value%";
                break;
            case '=':
            case '>':
            case '<':
                break;
            default:
                $operator = '=';
                break;
        }

        $asignaturasCoincidentes = Asignatura::query();
        // SELECT FROM Asignatura

        foreach ($fields as $campo) {
            $asignaturasCoincidentes = $asignaturasCoincidentes->orWhere(
                $campo,
                $operator,
                $value
            );
            // 1ª iteración: Select from asignatura where nombre like '%DWC%'
            // 2ª iteración: Select from asignatura
            // where nombre like '%DWC%' OR where horas like '%DWC%'

        }

        //activa log de querys
        DB::enableQueryLog();

        $result = $asignaturasCoincidentes->limit($request->limit)->get();

        // Muestra la última query ejecutada
        print_r(DB::getQueryLog());

        return $result->toJSON();

    }


    public function delete(Request $request, $id) {
        Asignatura::findOrFail($id)->delete();
    }
        // SELECT * FROM ASIGNATURA WHERE nombre like 'DWC' or where horas like '%DWC%' or where horas like '%DWC%'



}
