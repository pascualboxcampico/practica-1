<?php
namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Asignatura;

class AlumnoController extends BaseController
{
    public function getAll(Request $request) {
        return response()->json([
            'success' => true,
            'mensaje' => 'Obtengo todos los alumnos desde el controller',
            'data'    => DB::table('users')->get()
        ]);
    }

    public function deleteAlumno(Request $request, $id) {
        $user = DB::table('users')->where('id', $id)->first();
        if ($user === null) {
            return response()->json([
                'success' => false,
                'mensaje' => 'Usuario no encontrado',
                'data'    => null
            ], 404);
        }

        DB::table('users')->where('id', $id)->delete();
        return response()->json([
            'success' => true,
            'mensaje' => 'Usuario borrado correctamente',
            'data'    => $user
        ]);

    }

    public function getAlumno(Request $request, $id) {
        $user = DB::table('users')->where('id', $id)->first();

        return response()->json([
            'success' => true,
            'mensaje' => 'Obtengo un alumno concreto desde el controller',
            'data'    => $user

        ]);
    }

    public function getAsignaturaAlumno(Request $request, $id) {
        $user = User::find($id);
        if($user === null) return 'ERROR USER NOT EXISTS';

        $asignatura = $user->asignatura;
        if($asignatura === null) return 'ERROR USER no tiene LESSONS';

        return $user->asignatura->toJson();
    }

    public function getFichaAlumno(Request $request, $id) {
        return "Obtengo la ficha del alumno con id $id";
    }

    public function insert(Request $request) {
        $data = $request->only(['name', 'age', 'email', 'password']);

        $request->validate([
            'name' => 'required',
            'age' =>  'required|integer|digits_between:1,3'
        ]);

        try {
            DB::table('users')->insert($data);
            return response()->json([
                'success' => true,
                'mensaje' => 'GRANDE',
                'data'    => null
            ], 200);
        } catch(\Exception $e) {
            return response()->json([
                'success' => false,
                'mensaje' => $e->getMessage(),
                'data'    => $e->getTraceAsString(),
            ], 500);
        }

    }
}
