<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AsegurarIdNumerico
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // Recojo el parámetro {id} de la ruta
        $id = $request->route('id');

        // Si el id NO es numérico
        if (!is_numeric($id)) {
            // Envío cascazo y paro la ejecución con el "return"
            /*return response()->json([
                'message' => 'Mal desde el middleware'
            ], 422);*/

            // Se redirige al usuario a la ruta de error
            return redirect()->route('error');



        }
        
        // Si el id SÍ es numérico, le indico a la request que puede seguir su camino
        return $next($request);
    }
}
