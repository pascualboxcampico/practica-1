<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AlumnoController;
use App\Http\Controllers\AsignaturaController;
use App\Http\Middleware\AsegurarIdNumerico;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/', function (Request $request) {
    return $request->input('miparametro');
});

Route::get('error', function (Request $request) {
    return response()->json(['message' => 'Esto es un error'], 400);
})->name('error');

Route::prefix('alumno')->group(function () {
    Route::get('', [AlumnoController::class, 'getAll']);
    Route::post('', [AlumnoController::class, 'insert']);

    Route::middleware(AsegurarIdNumerico::class)->group(function() {
        Route::get(
            '/{id}', [AlumnoController::class, 'getAlumno']
        );

        Route::delete('/{id}', [AlumnoController::class, 'deleteAlumno']);

        Route::get(
            '/{id}/asignatura',
            [AlumnoController::class, 'getAsignaturaAlumno']
        );

        Route::get('/{id}/ficha', [AlumnoController::class, 'getFichaAlumno']);
    });
});

Route::get('asignatura', [AsignaturaController::class, 'getAll']);
Route::get('asignatura/search', [AsignaturaController::class, 'search']);
Route::post('asignatura', [AsignaturaController::class, 'create']);
Route::delete('asignatura/{id}', [AsignaturaController::class, 'delete']);

// Obtiene todos los alumnos matriculados en una determinada asignatura
Route::get('asignatura/{id}/alumnos', [AsignaturaController::class, 'getAlumnos']);
